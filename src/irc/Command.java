/*
 * Copyright (C) 2010  Edvin "nCrazed" Malinovskis
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package irc;

public enum Command {

	START("[!start X Y][Starts gather with Y teams of X players]"),
	JOIN("[!join X][Adds you to team X (User random or r to join random team)]"),
	LEAVE("[!leave][Removes you from your current team]"),
	STOP("[!stop][Stops current gather.]"), 
	MUMBLE("[!mumble][Shows our mumble details]"),
	HELP("[!help][Shows this help]"),
        NICK("[!nick][Changes the bot irc nickname]"),
	NONE("", false);
	
	public static final String PREFIX = "!";
	
	private String help;
	private boolean visible;
	
	Command(String help) {
		this(help, true);		
	}
	
	Command(String help, boolean visible) {
		this.help = help;
		this.visible = visible;		
	}
	
	public boolean isVisible() {
		return visible;
	}
	
	@Override
	public String toString() {
		return help;
	}

	public static Command toCommand(String str) {
		try {
			return valueOf(str.substring(PREFIX.length()).toUpperCase());
		} catch (Exception e) {
			return NONE;
		}
		
	}

}
