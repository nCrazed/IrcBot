/*
 * Copyright (C) 2010  Edvin "nCrazed" Malinovskis
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package irc.addons;

import irc.*;


public abstract class Addon {
	
	protected Bot parent;
	
	public Addon(Bot parent) {
		this.parent = parent;
	}
	
	public void parseMessage(String sender, String message) {
		if(message.startsWith(Command.PREFIX)) {
			parseCommand(sender, message.split(" "));
		}				
	}
	
	public abstract void parseCommand(String sender, String command[]);

	public void onNickChange(String oldNick, String newNick) {
	}

	public void onPart(String sender) {
	}

	public void onJoin(String sender) {		
	}	

}
