/*
 * Copyright (C) 2010  Edvin "nCrazed" Malinovskis
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package irc.addons.gather;

import java.util.ArrayList;

public class Team {
		
	private int size;
	private ArrayList<Player> players;
	
	public Team(int size) {
		this.size = size;
		players = new ArrayList<Player>();
	}
	
	public void add(String name) {
		if(players.size() < size) {
			this.players.add(new Player(name));
		}
	}
	
	public boolean remove(String name) {
		return players.remove(new Player(name));
	}
	
	public boolean hasPlayer(String name) {
		return players.contains(new Player(name));
	}
	
	public boolean isFull() {
		return players.size() == size;
	}
	
	@Override
	public String toString() {
		int current = players.size();
		int empty = size - current;
		String out = "";
		
		// List all players
		for(Player player : players) {
			out += player.toString() + " ";
		}
		
		// Fill empty slots
		for(int i=0; i<empty; i++) {
			out += "- ";
		}
		
		// Remove trailing " "
		out = out.substring(0, out.length()-1);
		
		// [- - - - -][0/5]
		return "[" + out + "][" + current + "/" + size + "]";
	}
	
}
