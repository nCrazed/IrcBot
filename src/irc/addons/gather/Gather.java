/*
 * Copyright (C) 2010  Edvin "nCrazed" Malinovskis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package irc.addons.gather;

import irc.Bot;
import irc.Command;
import irc.addons.Addon;

import java.util.Random;

public class Gather extends Addon {

    private boolean running;
    private Team[] teams;

    public Gather(Bot parent) {
        super(parent);
        this.running = false;
    }

    public void start(int teamSize) {
        start(teamSize, 2);
    }

    public void start(int teamSize, int teamCount) {
        if (teamSize == 0 || teamCount == 0) { // That would be silly
            parent.sendMessage("Invalid team size or team count.");
            return;
        }

        this.teams = new Team[teamCount];

        for (int i = 0; i < teamCount; i++) {
            teams[i] = new Team(teamSize);
        }

        this.running = true;
        parent.sendMessage(toString());
    }

    public void stop() {
        this.teams = new Team[0];
        this.running = false;
        parent.sendMessage(toString());
    }

    public void add(String playerName, int teamId) {
        Team team = teams[teamId];

        if (!team.isFull()) {
            team.add(playerName);
            parent.sendMessage(toString());
        } else {
            parent.sendMessage("Team is already full.");
            return;
        }

    }

    public void add(String playerName, String team) {
        // Make sure player is not on any team
        for (Team t : teams) {
            if (t.remove(playerName)) {
                break;
            }
        }

        int id;    // Used to call add(String, int)

        try {
            // Assume numeric team id
            id = Integer.parseInt(team);
            id--;

        } catch (NumberFormatException e) {
            // Is it random?
            if (team.equalsIgnoreCase("random") || team.equalsIgnoreCase("r")) {
                int tries = 0;
                int maxTries = teams.length * 2;
                Random r = new Random();

                while (true) {
                    id = r.nextInt(teams.length);

                    if (!teams[id].isFull()) {
                        break;
                    } else if (tries == maxTries) {
                        parent.sendMessage("Couldn't find an empty team.");
                        return;
                    }
                    tries++;
                }
            } else {
                parent.sendMessage("Invalid team id.");
                return;
            }
        }

        if (id >= 0 && id < teams.length) {
            add(playerName, id);
        } else {
            parent.sendMessage("Invalid team id.");
            return;
        }
    }

    public void remove(String playerName) {
        for (Team team : teams) {
            if (team.remove(playerName)) {
                parent.sendMessage(toString());
            }
        }
    }

    @Override
    public void onJoin(String sender) {
        if (running) {
            parent.sendNotice(sender, toString());
        }
    }

    @Override
    public void onPart(String sender) {
        remove(sender);
    }

    @Override
    public void onNickChange(String oldNick, String newNick) {
        if (running) {
            for (Team team : teams) {
                if (team.hasPlayer(oldNick)) {
                    team.remove(oldNick);
                    team.add(newNick);
                    parent.sendMessage(toString());
                }
            }
        }
    }

    @Override
    public String toString() {
        if (!running) {
            return "Gather is stopped.";
        }

        String message = "";
        for (Team team : teams) {
            message += team.toString() + " ";
        }

        return message;
    }

    @Override
    public void parseCommand(String sender, String[] command) {

        switch (Command.toCommand(command[0])) {
            case START:
                // Get some defaults
                int teamCount = 2;
                int teamSize = 2;

                // See if they need to be changed
                try {
                    teamSize = Integer.parseInt(command[1]);
                    teamCount = Integer.parseInt(command[2]);
                } catch (NumberFormatException e) {
                    parent.sendMessage("Invalid team size or team count.");
                    return;
                } catch (ArrayIndexOutOfBoundsException e) {
                    // missing arguments, go with defaults.
                }

                // Start the gather
                start(teamSize, teamCount);
                break;


            case STOP:
                if (running) {
                    stop();
                }
                break;

            case JOIN:
                if (running) {
                    add(sender, command[1]);
                }
                break;

            case LEAVE:
                if (running) {
                    remove(sender);
                }
                break;

            default:
                break;
        }

    }
}