/*
 * Copyright (C) 2010  Edvin "nCrazed" Malinovskis
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package irc.addons;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import irc.Bot;
import irc.Command;

public class Info extends Addon {

    public Info(Bot parent) {
        super(parent);
    }

    @Override
    public void parseCommand(String sender, String[] command) {

        switch (Command.toCommand(command[0])) {
            case MUMBLE:
                try {
                    Properties p = new Properties();
                    p.load(new FileInputStream("info.cfg"));
                    parent.sendNotice(sender, "Address: " + p.getProperty("mumbleAddress"));
                    parent.sendNotice(sender, "Port: " + p.getProperty("mumblePort"));
                } catch (FileNotFoundException e) {
                    System.out.println("ERROR: info.cfg missing");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            case HELP:
                for (Command c : Command.values()) {
                    if (c.isVisible()) {
                        parent.sendNotice(sender, c.toString());
                    }
                }
                break;

            default:
                break;
        }
    }
}
