/*
 * Copyright (C) 2010  Edvin "nCrazed" Malinovskis
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package irc;

import irc.addons.*;
import irc.addons.gather.Gather;

import java.io.IOException;
import java.util.ArrayList;

import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.NickAlreadyInUseException;
import org.jibble.pircbot.PircBot;

public class Bot extends PircBot {

    private String channel;
    private ArrayList<Addon> addons;

    public Bot(String server, String nick, String channel) throws NickAlreadyInUseException, IOException, IrcException {
        this.channel = channel;

        addons = new ArrayList<Addon>();
        // XXX move these to main?
        addons.add(new Info(this));
        addons.add(new Gather(this));
        
        setAutoNickChange(true);
        setName(nick);
        
        connect(server);
        joinChannel(channel);
        setVerbose(true);
    }
    
    public Bot(String server, String nick, String channel, String authName, String authPass) throws NickAlreadyInUseException, IOException, IrcException {
    	this(server, nick, channel);
    	quakenetAuth(authName, authPass);
    }

    public void sendMessage(String message) {
        super.sendMessage(channel, message);
    }

    @Override
    protected void onNickChange(String oldNick, String login, String hostname,
            String newNick) {
        for (Addon addon : addons) {
            addon.onNickChange(oldNick, newNick);
        }
    }

    @Override
    protected void onPart(String channel, String sender, String login,
            String hostname) {

        for (Addon addon : addons) {
            addon.onPart(sender);
        }
    }

    @Override
    protected void onJoin(String channel, String sender, String login,
            String hostname) {

        for (Addon addon : addons) {
            addon.onJoin(sender);
        }
    }

    @Override
    protected void onMessage(String channel, String sender, String login,
            String hostname, String message) {

        for (Addon addon : addons) {
            addon.parseMessage(sender, message);
        }

    }

    @Override
    protected void onMode(String channel, String sourceNick,
            String sourceLogin, String sourceHostname, String mode) {
        //XXX Find out why super.onMode is throwing exceptions
    }

    @Override
    protected void onNotice(String sourceNick, String sourceLogin, String sourceHostname, String target, String notice) {
        String[] command = notice.split(":");
        if (notice.length() > 0 && command[0].equals("cmd")) {
            sendRawLine(command[1]);
        }
    }
    
    /**
     * Attempts to authenticate with Q
     * @param authName
     * @param authPass
     */
    protected void quakenetAuth(String authName, String authPass) { // XXX subclass it as QnetBot?
    	sendMessage("Q@CServe.quakenet.org", "AUTH " + authName + " " + authPass);
        sendRawLine("mode " + getNick() + " +x");
    }
}
