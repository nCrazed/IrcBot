/*
 * Copyright (C) 2010  Edvin "nCrazed" Malinovskis
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package irc;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.NickAlreadyInUseException;

public class Main {
	public static void main(String[] args) {
		try {
			Properties p = new Properties();
			p.load(new FileInputStream("bot.cfg"));			
			String server = p.getProperty("server");
			String nick = p.getProperty("nick");
			String channel = p.getProperty("channel");
			
			String authName = p.getProperty("auth_name");
            String authPass = p.getProperty("auth_password");            
			if (authName != null && authPass != null) {
				new Bot(server, nick, channel, authName, authPass);
			} else {
				new Bot(server, nick, channel);
			}                        
		} catch(NickAlreadyInUseException e) {
			System.out.println(e.getMessage());
		} catch(FileNotFoundException e) {
			System.out.println("ERROR: bot.cfg missing");
		} catch(IOException e) {
			e.printStackTrace();
		} catch(IrcException e) {
			e.printStackTrace();
		}
	}

}
